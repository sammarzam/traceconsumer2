package es.upm.dit.apsv.traceconsumer.Repository;

import org.springframework.data.repository.CrudRepository;
import es.upm.dit.apsv.traceconsumer.model.TransportationOrder;


public interface TransportationOrderRepository extends CrudRepository<TransportationOrder,String> {
    //TransportationOrder findByTruckAndSt(String truck, int st);
}
