package es.upm.dit.apsv.traceconsumer;

import java.util.Optional;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import es.upm.dit.apsv.traceconsumer.model.*; //incorporo ambos
import es.upm.dit.apsv.traceconsumer.Repository.*;

@SpringBootApplication
public class TraceconsumerApplication {

    public static final Logger log = LoggerFactory.getLogger(TraceconsumerApplication.class);

    @Autowired
	private TraceRepository traceRepository;

    @Autowired
        private  TransportationOrderRepository orderRepository; //incluyo el orderRepository

    @Autowired
    private  Environment env;

    public static void main(String[] args) {
        SpringApplication.run(TraceconsumerApplication.class, args);
    }
    

    @Bean("consumer")
    public Consumer<Trace> checkTrace() {
            return t -> {
                    t.setTraceId(t.getTruck() + t.getLastSeen());
                    traceRepository.save(t);
                    TransportationOrder result = orderRepository.findById(t.getTruck()).get();
                    if (result != null && result.getSt() == 0) {
                            result.setLastDate(t.getLastSeen());
                            result.setLastLat(t.getLat());
                            result.setLastLong(t.getLng());
                            if (result.distanceToDestination() < 10)
                                    result.setSt(1);
                        orderRepository.save(result);
                            log.info("Order updated: "+ result);
                    }

            };

    }


}
